﻿    using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Site.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Site.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IActionResult Index()
        {
            PartViewIndex part = new PartViewIndex(null);
            return View(part);
        }

        [HttpPost]
        public IActionResult Index(int? request)
        {
            PartViewIndex part = new PartViewIndex(request);
            return View(part);
        }

        [HttpGet]
        public IActionResult Projects()
        {
            PartViewIndex part = new PartViewIndex(null);
            ViewBag.part = part;
            return View();
        }

        [HttpPost]
        public IActionResult Projects(int? request)
        {

            PartViewIndex part = new PartViewIndex(request);
            ViewBag.part = part;
            return View();
        }

        [HttpGet]
        public IActionResult About()
        {
            return View();
        }

        [HttpPost]
        public IActionResult About(int? request)
        {
            PartViewIndex part = new PartViewIndex(request);
            return View(part);
        }

        [HttpGet]
        public IActionResult Contacts()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Contacts(bool partial)
        {
            return View();
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
