﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Site.Models;

namespace Site.Controllers
{
    public class AccountController : Controller
    {
        private UserManager<UserModel> UserManager { get; set; }
        private SignInManager<UserModel> SignInManager { get; set; }

        public AccountController(UserManager<UserModel> userManager, SignInManager<UserModel> signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public IActionResult Registration()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> RegistrationUser([FromBody] RegistrationModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new UserModel { UserName = model.Name, Email = model.Email };
                Instances.Instance.db.CreateNewUser(user.Id, model.Name);
                await UserManager.CreateAsync(user, model.Password);

                return Json(new { result = "redirect", value = Url.Action("Index", "Home") });
            }
            else
            {
                return Json(new { result = "error", value = "Model Start is not valid" });
            }
        }

        public IActionResult Login()
        {
            return View();
        }

        [Authorize]
        public async Task<IActionResult> Logout()
        {
            await SignInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public async Task<IActionResult> LoginUser([FromBody] LoginModel model)
        {
            if (ModelState.IsValid)
            {
                await SignInManager.PasswordSignInAsync(model.Name, model.Password, false, false);

                return Json(new { result = "redirect", value = Url.Action("Index", "Home") });
            }
            else
            {
                return Json(new { result = "error", value = "Model Start is not valid" });
            }
        }
    }
}