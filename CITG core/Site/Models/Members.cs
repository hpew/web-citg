﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Site.Models
{
    public class Members
    {
        [BsonId]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Role { get; set; }
        public string Description { get; set; }
        public string Email { get; set; }
        public string LinkToImg { get; set; }
        public string Phrase { get; set; }
        public string PhoneNumber { get; set; }
    }
}