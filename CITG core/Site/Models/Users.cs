﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Site.Models
{
    public class Users
    {
        public ObjectId _id { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
    }
}
