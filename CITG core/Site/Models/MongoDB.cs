﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Site.Models
{
    public class Instances
    {
        public static Instances Instance;
        public MongoDB db;

        public Instances(IConfiguration configuration)
        {
            Instance = this;
            db = new MongoDB(configuration.GetConnectionString("mongodatabase"));
        }

        public Users GetUser(HttpContext context)
        {
            string userId;
            try { userId = context.User.FindFirst(ClaimTypes.NameIdentifier).Value; }
            catch { return new Users(); }
            return Instance.db.GetUser(userId);
        }
    }

    public class MongoDB
    {
        private MongoClient client;
        private IMongoDatabase database;
        public MongoDB(string path)
        {
            client = new MongoClient(path);
            database = client.GetDatabase("DataBaseDoors");
        }

        public void CreateNewUser(string id, string name)
        {
            Users user = new Users();
            user.UserId = id;
            user.Name = name;

            IMongoCollection<Users> collection = database.GetCollection<Users>("localuser");

            collection.InsertOne(user);
        }

        public Users GetUser(string id)
        {
            IMongoCollection<Users> collection = database.GetCollection<Users>("localuser");

            var Builder = Builders<Users>.Filter;
            var filter = Builder.Eq("Id", id);
            return collection.Find(filter).FirstOrDefault();
        }
    }
}
