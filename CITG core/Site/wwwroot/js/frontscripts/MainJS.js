﻿
//$('.nav-toggle').on('click', function () {
//    $('#menu').toggleClass('active');
//    $('.CITG-logo-conteiner').toggleClass('active');
//    $('#CITG-logo-nextelement-green-text').toggleClass('logo-green-text');
//    //$('#CITG-logo-big-text').toggleClass('active');
//});

//============================================ [ BACKEND ] ============================================ 

var $PartViewMain = 1;
var $PartViewProject = 1;
var $AjaxRequest = true;

$(window).on('load', function () {
    //alert(this.window.location);

    if (window.location.href.indexOf("Home/Projects") > -1) {
        this.removeclass();
        $("#projects").addClass("active");
        this.ajaxprojectspage();
    }

    if (window.location.href.indexOf("Home/About") > -1) {
        this.removeclass();
        $("#about").addClass("active");
    }

    if (window.location.href.indexOf("Home/Contacts") > -1) {
        this.removeclass();
        $("#contacts").addClass("active");
    }

    if (window.location.href.indexOf("Home/Index") > -1 || window.location.href == "https://" + this.window.location.host + "/") {
        ajaxmainpage();
    }
});

$("#logo-mainpage").click(function () {
    $("#content").html("");
    ajaxlinkto("/Home/Index", "#mainpage");
    window.history.pushState({ href: "/", tab: "#mainpage" }, null, "/");
});

$("#mainpage").click(function () {
    $("#content").html("");
    ajaxlinkto("/Home/Index", "#mainpage");
    window.history.pushState({ href: "/", tab: "#mainpage" }, null, "/");
});

$("#projects").click(function () {
    $("#content").html("");
    ajaxlinkto("/Home/Projects", "#projects");
    window.history.pushState({ href: "/Home/Projects", tab: "#projects" }, null, "/Home/Projects");
});

$("#about").click(function () {
    
    $("#content").html("");
    ajaxlinkto("/Home/About", "#about");
    window.history.pushState({ href: "/Home/About", tab: "#about" }, null, "/Home/About");
});

$("#contacts").click(function () {
    
    $("#content").html("");
    ajaxlinkto("/Home/Contacts", "#contacts");
    window.history.pushState({ href: "/Home/Contacts", tab: "#contacts" }, null, "/Home/Contacts");
});

$("#scroll").click(function () {
        $("html, body").animate({ scrollTop: 0 }, 600);
});

$("body").scroll(function () {
    var nextscroll = $("body").scrollTop();

    if (nextscroll > 500) {
        $("#scroll").fadeIn();
    }
    else{
        $("#scroll").fadeOut();
    }
});

$(document).ready(function () { 
    window.addEventListener('popstate', function (e) {
        if (e.state) {
            if (e.state.tab) {
                ajaxlinkto(e.state.href, e.state.tab);
            }
            else {
                ajaxlinkto(e.state.href, "#mainpage");
            }
        }
    });   
});

function removeclass() {
    $("#mainpage").removeClass("active");
    $("#projects").removeClass("active");
    $("#about").removeClass("active");
    $("#contacts").removeClass("active");
}

function startpreloader() {
    document.getElementById("conteiner-preloader").style.display = 'block';
    document.getElementById("conteiner-preloader").style.opacity = 1;
    document.getElementsByClassName("elements-preloaders")[0].style.opacity = 1;
    document.getElementsByClassName("elements-preloaders")[1].style.opacity = 1;
    document.getElementsByClassName("elements-preloaders")[2].style.opacity = 1;
}

function ajaxmainpage() {

    $.ajax({
        url: "/Home/Index",
        type: "POST",
        data: "request=" + $PartViewMain,
        success: function (response) {

            if ($AjaxRequest == true) {
                $("#content").append(response);
            }

            if ($PartViewMain < 3) {
                $PartViewMain++;
                ajaxmainpage();
            }
        },
    });
}

function ajaxaboutpage($id) {

    $.ajax({
        url: "/Home/About",
        type: "POST",
        data: "request=" + $id,
        success: function (response) {
            if ($AjaxRequest == true) {
                $("#content").append(response);
            }
        },
    });
}

function ajaxprojectspage() {

    $.ajax({
        url: "/Home/Projects",
        type: "POST",
        data: "request=" + $PartViewProject,
        success: function (response) {
            if ($AjaxRequest == true) {
                $("#content").append(response);
            }

            if ($PartViewProject < 1) {
                $PartViewProject++;
                ajaxprojectspage();
            }
        },
    });
}

function ajaxlinkto(URL, tab) {
    $AjaxRequest = false;
    $.ajax({
        url: URL,
        type: "POST",
        data: "partial=" + true,
        success: function (response) {
            removeclass();
            $(tab).addClass("active");

            setTimeout(function () {
                document.getElementById("conteiner-preloader").style.display = 'none';
            }, 1500);
            document.getElementById("conteiner-preloader").style.opacity = 0;
            document.getElementsByClassName("elements-preloaders")[0].style.opacity = 0;
            document.getElementsByClassName("elements-preloaders")[1].style.opacity = 0;
            document.getElementsByClassName("elements-preloaders")[2].style.opacity = 0;

            $("#content").html(response);

            if (URL == "/Home/Index" || URL == "/") {
                $PartViewMain = 1;
                ajaxmainpage();
                $AjaxRequest = true;
            }

            if (URL == "/Home/Projects") {
                $PartViewProject = 1;
                ajaxprojectspage();
                $AjaxRequest = true;
            }

            try {
                if (document.getElementsByClassName("icon")[0].classList.contains("active")) {
                    document.getElementsByClassName("icon")[0].classList.remove("active");
                    document.getElementById("icon-div").classList.remove("active");
                    document.getElementById("menu").classList.remove("active");
                }
            }
            catch {

            }
        },
    });
}