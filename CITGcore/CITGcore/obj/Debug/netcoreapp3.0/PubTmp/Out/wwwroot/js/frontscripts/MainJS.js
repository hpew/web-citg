﻿var $PartViewMain = 1;
var $PartViewProject = 1;
var $AjaxRequest = true;

window.addEventListener("load", function () {
    if (window.location.href.indexOf("Projects") > -1) {
        removeclass();
        $("#projects").addClass("active");
    }

    if (window.location.href.indexOf("About") > -1) {
        removeclass();
        $("#about").addClass("active");
    }

    if (window.location.href.indexOf("Contacts") > -1) {
        removeclass();
        $("#contacts").addClass("active");
    }

    if (window.location.href.indexOf("Account") > -1) {
        removeclass();
        $("#account").addClass("active");
    }

    window.addEventListener("popstate", function (e) {
        if (e.state) {
            ajaxlinkto(e.state.href, e.state.tab);
        }
        else {
            ajaxlinkto(e.state.href, "#mainpage");
        }
    });
});


    function removeclass() {
        $("#mainpage").removeClass("active");
        $("#projects").removeClass("active");
        $("#about").removeClass("active");
        $("#contacts").removeClass("active");
        $("#login").removeClass("active");
        $("#account").removeClass("active");
    }

function ajaxlinkto(URL, tab) {
    $AjaxRequest = true;
    $.ajax({
        url: URL,
        type: "POST",
        data: "partial=" + true,
        success: function (response) {
            if (response.result == "error") {
                alert("Произошла ошибка!\n" + response.value);
                return;
            }

            removeclass();
            if (tab != "")
                $(tab).addClass("active");

            if ($AjaxRequest = true) {
                $("#content").html(response);
                $('body').scrollTop(0);
                window.history.pushState({ href: URL, tab: tab }, tab, URL);
            }

            try {
                if (document.getElementsByClassName("icon")[0].classList.contains("active")) {
                    document.getElementsByClassName("icon")[0].classList.remove("active");
                    document.getElementById("icon-div").classList.remove("active");
                    document.getElementById("menu").classList.remove("active");
                }
            }
            catch {

            }
        },
    });
    }
