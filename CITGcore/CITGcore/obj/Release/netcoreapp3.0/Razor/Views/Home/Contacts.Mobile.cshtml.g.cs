#pragma checksum "C:\Users\Jaka2\source\repos\CITGcore\CITGcore\Views\Home\Contacts.Mobile.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "4c04d446ec11783b4d3501bdcb937e3eb5404012"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Contacts_Mobile), @"mvc.1.0.view", @"/Views/Home/Contacts.Mobile.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\Jaka2\source\repos\CITGcore\CITGcore\Views\_ViewImports.cshtml"
using CITGcore;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Jaka2\source\repos\CITGcore\CITGcore\Views\_ViewImports.cshtml"
using CITGcore.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"4c04d446ec11783b4d3501bdcb937e3eb5404012", @"/Views/Home/Contacts.Mobile.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"57943a56e115203bce878f35757920db8c09c615", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Contacts_Mobile : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/css/image/kartashov.png.png"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "C:\Users\Jaka2\source\repos\CITGcore\CITGcore\Views\Home\Contacts.Mobile.cshtml"
  
    
    if(ViewBag.Layout == 1)
    {
        Layout = "_Layout.Mobile";
    }

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<style>
    section.paddingcontent {
        padding-bottom: 100px;
        padding-left: 20px;
        padding-right: 20px;
    }

    section.paddingheader {
        padding-left: 200px;
        padding-right: 200px;
        padding-top: 62px;
        background-color: #555;
    }

    div.block {
        padding-left: 80px;
        padding-right: 80px;
        padding-top: 40px;
        padding-bottom: 40px;
        border: 1px solid #444;
        border-radius: 3px;
        background-color: #222;
    }

    div.flexblock {
        display: flex;
        flex-wrap: wrap;
        justify-content: space-evenly;
    }

    div.singleblock {
        text-align: center;
        flex-basis: 100%;
        min-width: 600px;
        margin: 20px;
    }

    div.doubleblock {
        text-align: center;
        min-width: 600px;
        flex: 1 1 calc(50% - 200px);
        margin: 20px;
    }

    div.tripleblock {
        text-align: center;
        min-width: 600px;");
            WriteLiteral(@"
        flex: 1 1 calc(33.33% - 40px);
        margin: 20px;
    }

    div.one-thirds {
        text-align: center;
        min-width: 600px;
        flex: 1 1 calc(33.33% - 40px);
        margin: 20px;
    }

        div.one-thirds iframe {
            align-content: end;
            width: 100%;
            height: 300px;
        }

    div.two-thirds {
        text-align: center;
        min-width: 300px;
        flex: 1 1 calc(66.66% - 40px);
        margin: 20px;
    }

    div.verticalalign {
        margin: auto;
    }

    div.employee {
        margin-top: 100px;
    }

        div.employee div.doubleblock h2 {
            font-size: 34px;
        }

        div.employee div.doubleblock h4 {
            color: forestgreen;
            font-size: 24px;
        }

        div.employee div.doubleblock h5 {
            font-size: 24px;
        }

        div.employee div.doubleblock p {
            font-size: 20px;
            margin-bottom: 0px;
       ");
            WriteLiteral(@"     margin-top: 20px;
        }

        div.employee div.doubleblock img {
            width: 420px;
        }
</style>

<section class=""paddingheader paddingcontent"">
    <div class=""flexblock employee"">
        <div class=""doubleblock imgblock"">
            ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "4c04d446ec11783b4d3501bdcb937e3eb54040126113", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
        </div>

        <div class=""doubleblock verticalalign textblock"">
            <h2>Карташов Олег Олегович</h2>
            <h4>Директор центра IT притяжения</h4>

            <p>Рабочий номер телефона:</p>
            <h5>8 (999) 999 99 99</h5>

            <p>Электронная почта:</p>
            <h5>centerit");
            WriteLiteral("@gmail.com</h5>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"flexblock employee\">\r\n        <div class=\"doubleblock imgblock\">\r\n            ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "4c04d446ec11783b4d3501bdcb937e3eb54040127657", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
        </div>

        <div class=""doubleblock verticalalign textblock"">

            <h2>Карташов Олег Олегович</h2>
            <h4>Директор центра IT притяжения</h4>

            <p>Рабочий номер телефона:</p>
            <h5>8 (999) 999 99 99</h5>

            <p>Электронная почта:</p>
            <h5>centerit");
            WriteLiteral("@gmail.com</h5>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"flexblock employee\">\r\n        <div class=\"doubleblock imgblock\">\r\n            ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "4c04d446ec11783b4d3501bdcb937e3eb54040129203", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
        </div>

        <div class=""doubleblock verticalalign textblock"">

            <h2>Карташов Олег Олегович</h2>
            <h4>Директор центра IT притяжения</h4>

            <p>Рабочий номер телефона:</p>
            <h5>8 (999) 999 99 99</h5>

            <p>Электронная почта:</p>
            <h5>centerit");
            WriteLiteral("@gmail.com</h5>\r\n        </div>\r\n    </div>\r\n</section>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
