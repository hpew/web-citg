#pragma checksum "C:\Users\Jaka2\source\repos\CITGcore\CITGcore\Views\Account\СonfirmUser.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "667398c41777d507a740d5b6967a3cf4b4a4c533"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Account_СonfirmUser), @"mvc.1.0.view", @"/Views/Account/СonfirmUser.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\Jaka2\source\repos\CITGcore\CITGcore\Views\_ViewImports.cshtml"
using CITGcore;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Jaka2\source\repos\CITGcore\CITGcore\Views\_ViewImports.cshtml"
using CITGcore.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"667398c41777d507a740d5b6967a3cf4b4a4c533", @"/Views/Account/СonfirmUser.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"57943a56e115203bce878f35757920db8c09c615", @"/Views/_ViewImports.cshtml")]
    public class Views_Account_СonfirmUser : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<List<Users>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("elements_image"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/css/image/newuser.png"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/js/frontscripts/MainJS.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 3 "C:\Users\Jaka2\source\repos\CITGcore\CITGcore\Views\Account\СonfirmUser.cshtml"
  
    ViewData["Title"] = "СonfirmUser";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<style>
    p {
        margin: 0;
        padding: 0;
    }

    #parentcontainer {
        position: relative;
        width: 100%;
    }

    #container {
            position: absolute;
            width: 100%;
            min-width: 300px;
            height: auto;
            transform:translate(-50%,calc(-100px - 50%));
            left:50%;
            top:50%;
        }

    #list {
            width: 80%;
            margin: 0 10%;
            position: relative;
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
        }
        #list::after,#list:before{
            content:"""";
            position:absolute;
            background:#c4c4c4;
            width:1px;
            height:calc(100% - 20px);
            top:20px;
        }
        #list::after{
            left:0;
        }
        #list::before{
            right:0;
        }
        .elements {
            width: 260px;
            height: 90px;
  ");
            WriteLiteral(@"          background-color: #c4c4c4;
            margin: 20px;
            position: relative;
            transition: 0.1s all linear 0.05s;
        }
        .elements_active{
            box-shadow: 0 0 16px #bf8c30, 0 0 8px #bf8c30, 0 0 4px #bf8c30,0 0 1px #b57a0f inset;
            transform: scale(1.05);
        }
        .elements_active::after{
            content:"""";
            position:absolute;
            bottom:-10px;
            left:2.5%;
            width:95%;
            height:2px;
            background:#b57a0f;
        }
            .elements:hover {
                box-shadow: 0 0 16px #ffffff, 0 0 8px #ffffff, 0 0 4px #ffffff,0 0 1px #000 inset;
                transform: scale(1.05);
            }
        .elements_image_container {
            position: absolute;
            left: 0;
            top: 0;
            width: 90px;
            height: 90px;
            overflow: hidden;
        }
        .elements_image {
            position:absolute;
    ");
            WriteLiteral(@"        left:50%;
            top:50%;
            transform:translate(-50%,-50%);
        }
        .elements_name_conteiner {
            position: absolute;
            left: 90px;
            top: 0;
            width: 170px;
            height: 45px;
            overflow: hidden;
        }
        .elements_name {
        }
        .elements_data_conteiner {
            position: absolute;
            left: 90px;
            top: 45px;
            width: 170px;
            height: 45px;
            overflow: hidden;
        }

        .elements_name_conteiner_new
        {
            position: absolute;
            left: 90px;
            top: 50%;
            width: 170px;
            height: 80px;
            overflow: hidden;
            transform: translateY(-50%);
        }

        .elements_data {
        }
        .elements_name, .elements_data {
            position: absolute;
            top: 50%;
            left: 5px;
            width: 140px;
       ");
            WriteLiteral(@"     transform: translateY(-50%);
            overflow: hidden;
            text-align: center;
            font-size: 17px;
        }
        #line,#line_2{
            position:relative;
            height:1px;
            width:calc(80% - 40px);
            margin:20px auto;
            background:#c4c4c4;
        }
</style>

<div id=""parentcontainer"">
    <div id=""container"">
        <div id=""line_2""></div>
        <div id=""list"">
");
#nullable restore
#line 144 "C:\Users\Jaka2\source\repos\CITGcore\CITGcore\Views\Account\СonfirmUser.cshtml"
             foreach (var item in Model)
            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                <div class=\"elements\"");
            BeginWriteAttribute("value", " value=\"", 3692, "\"", 3711, 1);
#nullable restore
#line 146 "C:\Users\Jaka2\source\repos\CITGcore\CITGcore\Views\Account\СonfirmUser.cshtml"
WriteAttributeValue("", 3700, item.Email, 3700, 11, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\r\n                    <div class=\"elements_image_container\">\r\n                        <img class=\"elements_image\"");
            BeginWriteAttribute("src", " src=\"", 3826, "\"", 3847, 1);
#nullable restore
#line 148 "C:\Users\Jaka2\source\repos\CITGcore\CITGcore\Views\Account\СonfirmUser.cshtml"
WriteAttributeValue("", 3832, item.LinkToImg, 3832, 15, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" />\r\n                    </div>\r\n                    <div class=\"elements_name_conteiner\">\r\n                        <p class=\"elements_name\">");
#nullable restore
#line 151 "C:\Users\Jaka2\source\repos\CITGcore\CITGcore\Views\Account\СonfirmUser.cshtml"
                                            Write(item.Name);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\r\n                    </div>\r\n                    <div class=\"elements_data_conteiner\">\r\n                        <p class=\"elements_data\">");
#nullable restore
#line 154 "C:\Users\Jaka2\source\repos\CITGcore\CITGcore\Views\Account\СonfirmUser.cshtml"
                                            Write(item.Role);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\r\n                    </div>\r\n                </div>\r\n");
#nullable restore
#line 157 "C:\Users\Jaka2\source\repos\CITGcore\CITGcore\Views\Account\СonfirmUser.cshtml"
            }

#line default
#line hidden
#nullable disable
            WriteLiteral("            <div id=\"newuser\" class=\"elements\">\r\n                <div class=\"elements_image_container\">\r\n                    ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "667398c41777d507a740d5b6967a3cf4b4a4c53310417", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
                </div>
                <div class=""elements_name_conteiner_new"">
                    <p class=""elements_name"">Пригласить нового пользователя</p>
                </div>
            </div>
        </div>
        <div id=""line""></div>
    </div>
</div>

");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "667398c41777d507a740d5b6967a3cf4b4a4c53311813", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"

<script>
    
    window.addEventListener('load', function () {

        var footer = document.querySelector("".footer-container"");

        var parent = document.querySelector(""#parentcontainer"");

        var re_size_container_p = function () {
            parent.style.height = (window.innerHeight - footer.clientHeight) + ""px"";
        };
        re_size_container_p();
        window.addEventListener('resize', re_size_container_p);

    });

    window.addEventListener('load', function () {
        var image = document.querySelectorAll('.elements_image');
        for (var i = 0; i < image.length; i++) {
            if (image[i].clientWidth > image[i].clientHeight) {
                image[i].style.height = ""100%"";
                image[i].style.width = ""auto"";
            }
            else {
                image[i].style.height = ""auto"";
                image[i].style.width = ""100%"";
            }
            image[i].addEventListener('load', function () {
                if ");
            WriteLiteral(@"(image[i].clientWidth > image[i].clientHeight) {
                    image[i].style.height = ""100%"";
                    image[i].style.width = ""auto"";
                }
                else {
                    image[i].style.height = ""auto"";
                    image[i].style.width = ""100%"";
                }
            });
        }
    });
</script>

<script>
    document.querySelector(""#newuser"").addEventListener('click', function () {
        window.location.href = ""/InviteUser"";
    });

    window.addEventListener('load', function () {
        const elements = document.querySelectorAll('.elements');

        for (var i = 0; i < elements.length; i++) {
            (function (index) {
                elements[index].addEventListener('click', function () {
                    var email = elements[index].value;
                    EditUser(email);

                    for (var i = 0; i < elements.length; i++) {
                        if (elements[i].classList.contains('eleme");
            WriteLiteral(@"nts_active')) elements[i].classList.remove('elements_active');
                    }
                    elements[index].classList.add('elements_active');
                });
            })(i);
        }
       
        function EditUser(Email) {
            alert(Email);
            $.ajax({
                url: ""/EditUser"",
                type: ""Post"",
                data: ""Email="" + Email,
                success: function (response) {
                    if (response.result == ""error"") {
                        $(""p#answer"").html(response.value);
                        $(""p#answer"").css(""color"", ""red"");
                    }
                    else {
                        $(""#content"").html(response);
                    }

                }
            });
        }
    });
</script>
");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<List<Users>> Html { get; private set; }
    }
}
#pragma warning restore 1591
