﻿var $PartViewMain = 1;
var $PartViewProject = 1;
var $AjaxRequest = true;
var listProjectIndexPage;

function startpreloader() {
    document.getElementById("conteiner-preloader").style.display = 'block';
    document.getElementById("conteiner-preloader").style.opacity = 1;
    document.getElementsByClassName("elements-preloaders")[0].style.opacity = 1;
    document.getElementsByClassName("elements-preloaders")[1].style.opacity = 1;
    document.getElementsByClassName("elements-preloaders")[2].style.opacity = 1;
}

function stoppreloader() {
    document.getElementById("conteiner-preloader").style.opacity = 0;
    document.getElementsByClassName("elements-preloaders")[0].style.opacity = 0;
    document.getElementsByClassName("elements-preloaders")[1].style.opacity = 0;
    document.getElementsByClassName("elements-preloaders")[2].style.opacity = 0;

    setTimeout(function () {
        document.getElementById("conteiner-preloader").style.display = 'none';
    }, 1500);
}


window.addEventListener("load", function () {
    stoppreloader();

    if (window.location.href.lastIndexOf("/") > -1) {
        listProjectIndexPage = document.querySelectorAll('.gallery-element');

        for (var i = 0; i < listProjectIndexPage.length; i++) {
            ((index) => {
                listProjectIndexPage[index].addEventListener('click', () => {
                    ajaxlinkto("/Projects", "#projects");
                    window.addEventListener('toProject', () => {
                        document.querySelectorAll('.Page_project_box_second-elements')[index].dispatchEvent(new Event('click'));
                    });
                });
            })(i);
        }
    }

    if (window.location.href.indexOf("Projects") > -1) {
        removeclass();
        $("#projects").addClass("active");
        window.dispatchEvent(new Event('toProject'));
    }

    if (window.location.href.indexOf("Blog") > -1) {
        removeclass();
    }

    if (window.location.href.indexOf("About") > -1) {
        removeclass();
        $("#about").addClass("active");
    }

    if (window.location.href.indexOf("Contacts") > -1) {
        removeclass();
        $("#contacts").addClass("active");
    }

    if (window.location.href.indexOf("Account") > -1) {
        removeclass();
        $("#account").addClass("active");
    }

    window.addEventListener("popstate", function (e) {
        var exit = document.getElementById("Page_project_box_second-exit-conteiner");
        if (exit) {
            exit.dispatchEvent(new Event('click'));
        }

        if (e.state) {
            ajaxlinkto(e.state.href, e.state.tab);
            console.log(e.state.href + "   " + e.state.tab);
            console.log(window.location.href);
        }
        else {
            ajaxlinkto("/", "#mainpage");
        }
    });

});


    function removeclass() {
        $("#mainpage").removeClass("active");
        $("#projects").removeClass("active");
        $("#about").removeClass("active");
        $("#contacts").removeClass("active");
        $("#login").removeClass("active");
        $("#account").removeClass("active");
    }

function ajaxlinkto(URL, tab) {
    var exit = document.getElementById("Page_project_box_second-exit-conteiner");
    if (exit) {
        exit.dispatchEvent(new Event('click'));
    }

    startpreloader();
    $AjaxRequest = true;
    $.ajax({
        url: URL,
        type: "POST",
        data: "partial=" + true,
        success: function (response) {
            stoppreloader();

            if (response.result == "error") {
                alert("Произошла ошибка!\n" + response.value);
                return;
            }
            
            removeclass();
            if (tab != "")
                $(tab).addClass("active");

            if ($AjaxRequest == true) {
                $("#content").html(response);
                $('body').scrollTop(0);
                window.history.pushState({ href: URL, tab: tab }, tab, URL);
            }

            if (URL == "/About") {
                Spays_Fly_to_spaus();
            }

            if (URL == "/") {
                listProjectIndexPage = document.querySelectorAll('.gallery-element');

                for (var i = 0; i < listProjectIndexPage.length; i++) {
                    ((index) => {
                        listProjectIndexPage[index].addEventListener('click', () => {
                            ajaxlinkto("/Projects", "#projects");
                            window.addEventListener('toProject', () => {
                                document.querySelectorAll('.Page_project_box_second-elements')[index].dispatchEvent(new Event('click'));
                            });
                        });
                    })(i);
                }
            }

            if (URL == "/Projects") {
                window.dispatchEvent(new Event('toProject'));
            }

            try {
                if (document.getElementsByClassName("icon")[0].classList.contains("active")) {
                    document.getElementsByClassName("icon")[0].classList.remove("active");
                    document.getElementById("icon-div").classList.remove("active");
                    document.getElementById("menu").classList.remove("active");
                }
            }

            catch(msg) {
                console.log(msg);
            }
        },
    });
    }