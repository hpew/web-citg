﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CITGcore.Models
{
    public abstract class Template
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public List<string> Paragraph { get; set; }
        public string Date { get; set; }
        public string MainImg { get; set; }
        public List<string> LinkVideo { get; set; }
        public List<string> LinkImg { get; set; }
    }
    public class Projects : Template
    {
    }
}
