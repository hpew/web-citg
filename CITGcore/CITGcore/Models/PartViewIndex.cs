﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CITGcore.Models
{
    public class PartViewIndex
    {
        public int? Part { get; set; }

        public PartViewIndex(int? value)
        {
            Part = value;
        }
    }
}
