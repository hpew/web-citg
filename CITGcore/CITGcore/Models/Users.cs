﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CITGcore.Models
{
    public class Users
    {
        public ObjectId _id { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
        public string Role { get; set; }
        public string Description { get; set; }
        public string Email { get; set; }
        public string LinkToImg { get; set; }  
        public string Phrase { get; set; }
        public string PhoneNumber { get; set; }
    }
}
