﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CITGcore.Models
{
    public class Instances
    {
        public static Instances Instance;
        public MongoDB db;

        public Instances(IConfiguration configuration)
        {
            Instance = this;
            db = new MongoDB(configuration.GetConnectionString("mongodbdatabase"));
        }

        public Users GetUser(HttpContext context)
        {
            string userId;
            try
            {
                userId = context.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            }
            catch
            {
                return new Users();
            }

            return Instance.db.GetUser(userId);
        }
    }

    public class MongoDB
    {
        private MongoClient client;
        private IMongoDatabase database;
        public MongoDB(string path)
        {
            client = new MongoClient(path);
            database = client.GetDatabase("citgdatabase");
        }

        public void CreateNewUser(string email)
        {
            Users user = new Users();
            user.Email = email;

            IMongoCollection<Users> collection = database.GetCollection<Users>("localuser");

            collection.InsertOne(user);
        }

        public void RegistrationUser(string email)
        {
            IMongoCollection<Users> collection = database.GetCollection<Users>("localuser");

            var Builder = Builders<Users>.Filter;
            var filter = Builder.Eq("Email", email);

            var update = Builders<Users>.Update.
                Set(n => n.UserId, Guid.NewGuid().ToString());

            collection.UpdateOne(filter, update);
        }

        public Users GetUser(string id)
        {
            IMongoCollection<Users> collection = database.GetCollection<Users>("localuser");

            var Builder = Builders<Users>.Filter;

            var filter = Builder.Eq("UserId", id);

            var user = collection.Find(filter).FirstOrDefault();

            return user;
        }

        public string GetUserName(string email)
        {
            IMongoCollection<Users> collection = database.GetCollection<Users>("localuser");

            var Builder = Builders<Users>.Filter;

            var filter = Builder.Eq("Email", email);

            var user = collection.Find(filter).FirstOrDefault();

            return user.Name;
        }

        public Users GetUserByName(string name)
        {
            IMongoCollection<Users> collection = database.GetCollection<Users>("localuser");

            var Builder = Builders<Users>.Filter;

            var filter = Builder.Eq("Name", name);

            var user = collection.Find(filter).FirstOrDefault();

            return user;
        }

        public Users GetUserByEmail(string email)
        {
            IMongoCollection<Users> collection = database.GetCollection<Users>("localuser");

            var Builder = Builders<Users>.Filter;

            var filter = Builder.Eq("Email", email);

            var user = collection.Find(filter).FirstOrDefault();

            return user;
        }

        public void UpdateUser(Users user)
        {
            //var user = GetUser(id);

            IMongoCollection<Users> collection = database.GetCollection<Users>("localuser");

            var Builder = Builders<Users>.Filter;

            var filter = Builder.Eq("Email", user.Email);

            var update = Builders<Users>.Update.
                Set(n => n.Name, user.Name).
                Set(n => n.Role, user.Role).
                Set(n => n.Description, user.Description).
                Set(n => n.LinkToImg, user.LinkToImg).
                Set(n => n.Phrase, user.Phrase).
                Set(n => n.PhoneNumber, user.PhoneNumber);

            collection.UpdateOne(filter, update);
        }

        public List<Users> GetCollectionUsers()
        {
            IMongoCollection<Users> collection = database.GetCollection<Users>("localuser");

            return collection.Find(new BsonDocument()).ToList();
        }

        public List<Users> GetCollectionUsersByPhone()
        {
            IMongoCollection<Users> collection = database.GetCollection<Users>("localuser");
            var filter = Builders<Users>.Filter.Ne("PhoneNumber", "");
            return collection.Find(filter).ToList();
        }
    }
}
