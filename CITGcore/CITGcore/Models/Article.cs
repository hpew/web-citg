﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CITGcore.Models
{
    public class Article
    {
        public string Author { get; set; }
        public string Title { get; set; }
        public List<string> Paragraph { get; set; }
        public List<string> LinkToImg { get; set; }
        public DateTime Date { get; set; }

        public Article() 
        {
            Author = string.Empty;
            Title = string.Empty;
            Paragraph = new List<string>();
            LinkToImg = new List<string>();
            Date = DateTime.Today;
        }
        public Article(string name)
        {
            if(name != null)
            {
                Author = name;
            }
        }
    }
}
