﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CITGcore.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace CITGcore.Controllers
{
    public class AccountController : Controller
    {
        private UserManager<Model> UserManager { get; set; }
        private SignInManager<Model> SignInManager { get; set; }
        private RoleManager<MyRole> RoleManager { get; set; }

        public AccountController(UserManager<Model> userManager, SignInManager<Model> signInManager, RoleManager<MyRole> roleManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
            RoleManager = roleManager;
        }

        [HttpGet]
        [Route("Account")]
        [Authorize]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [Route("Account")]
        [Authorize]
        public IActionResult Index(bool partial)
        {
            return PartialView();
        }


        [HttpGet]
        [Route("Registration")]
        public IActionResult Registration()
        {
            return View();
        }


        [HttpPost]
        [Route("Registration")]
        public IActionResult Registration(bool partial)
        {
            return PartialView();
        }


        [HttpPost]
        [Route("RegistrationUser")]
        public async Task<IActionResult> RegistrationUser([FromBody] RegistrationModel model)
        {
            if (ModelState.IsValid)
            {
                Users identified = Instances.Instance.db.GetUserByEmail(model.Email);

                if(identified != null)
                {
                    if(identified.UserId != null)
                    {
                        return Json(new { result = "error", value = "Пользователь уже зарегистрирован" });
                    }
                    else
                    {
                        var user = new Model { UserName = model.Email, Email = model.Email };

                        var account = await UserManager.CreateAsync(user, model.Password);

                        await UserManager.AddToRoleAsync(user, "User");

                        if (account.Succeeded)
                        {
                            
                            await SignInManager.PasswordSignInAsync(model.Email, model.Password, false, false);
                            return Json(new { result = "success", value = "Вы успешно зарегистрировались" });
                        }
                        else
                        {
                            return Json(new { result = "error", value = "Произошла непредвиденная ошибка" });
                        }
                    }
                }
                else
                {
                    return Json(new { result = "error", value = "Неизвестный пользователь" });
                }
            }
            else
            {
                return Json(new { result = "error", value = "Email или пароль введены некорректно" });
            }
        }


        [HttpGet]
        [Route("InviteUser")]
        [Authorize(Roles = "Admin")]
        public IActionResult InviteUser()
        {
            return View();
        }


        [HttpPost]
        [Route("InviteUser")]
        [Authorize(Roles = "Admin")]
        public IActionResult InviteUser([FromBody] InviteModel model)
        {
            if(ModelState.IsValid)
            {
                Users identified = Instances.Instance.db.GetUserByEmail(model.Email);

                if (identified == null)
                {
                    Instances.Instance.db.CreateNewUser(model.Email);

                    return Json(new { result = "redirect", value = Url.Action("Index", "Home") });
                }
                else
                {
                    return Json(new { result = "error", value = "Пользователь уже приглашён в команду" });
                }
            }
            else
            {
                return Json(new { result = "error", value = "Email введен некорректно" });
            }
        }

        [HttpGet]
        [Route("Login")]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [Route("Login")]
        public IActionResult Login(bool partial)
        {
            return PartialView();
        }


        [HttpPost]
        [Route("LoginUser")]
        public async Task<IActionResult> LoginUser([FromBody] LoginModel model)
        {
            if (ModelState.IsValid)
            {
                Users identified = Instances.Instance.db.GetUserByEmail(model.Email);

                if (identified != null)
                {
                    var valid = await SignInManager.PasswordSignInAsync(model.Email, model.Password, false, false);

                    if (valid.Succeeded)
                    {
                        if(identified.UserId == null)
                        {
                            return Json(new { result = "redirect", value = Url.Action("Сonfirm", "Account") });
                        }

                        return Json(new { result = "success", value = "Вы успешно авторизовались" });
                    }
                    return Json(new { result = "error", value = "Email или пароль введены некорректно" });
                }
                else
                {
                    return Json(new { result = "error", value = "Неизвестный пользователь" });
                }
            }
            else
            {
                return Json(new { result = "error", value = "Email или пароль введены некорректно" });
            }
        }

        [HttpGet]
        [Authorize]
        [Route("Confirm")]
        public IActionResult Сonfirm()
        {
            Users user = Instances.Instance.db.GetUserByEmail(HttpContext.User.Identity.Name);
            return View(user);
        }


        [HttpPost]
        [Authorize]
        [Route("Confirm")]
        public IActionResult Сonfirm([FromBody]Users user)
        {
            if (ModelState.IsValid)
            {
                Users getuser = Instances.Instance.db.GetUserByEmail(user.Email);

                if(getuser != null)
                {
                    if(user.UserId != null)
                    {
                        user.UserId = getuser.UserId;
                    }
                    else
                    {
                        user.UserId = Guid.NewGuid().ToString();
                    }
                    user.LinkToImg = getuser.LinkToImg;
                    Instances.Instance.db.UpdateUser(user);
                    return Json(new { result = "success", value = "Вы успешно изменили данные." });
                }

                return Json(new { result = "error", value = "Произошла ошибка. Пользователь не найден." });
            }

            return Json(new { result = "error", value = "Email или пароль введены некорректно" });
        }


        [Authorize(Roles = "Admin")]
        [Route("ConfirmUser")]
        public IActionResult СonfirmUser()
        {
            List<Users> users = Instances.Instance.db.GetCollectionUsers();

            return View(users);
        }


        [Authorize(Roles = "Admin")]
        [Route("EditUser")]
        [HttpPost]
        public IActionResult СonfirmUser(string Email)
        {
            Users user = Instances.Instance.db.GetUserByEmail(Email);
            return View("Confirm", user);
        }


        [HttpPost]
        [Authorize(Roles = "Admin")]
        [Route("EditUser")]
        public IActionResult EditUser([FromBody]Users user)
        {
            if (ModelState.IsValid)
            {
                Users getuser = Instances.Instance.db.GetUserByEmail(user.Email);

                if (getuser != null)
                {
                    user.UserId = getuser.UserId;
                    Instances.Instance.db.UpdateUser(user);
                    return Json(new { result = "success", value = "Вы успешно изменили данные." });
                }

                return Json(new { result = "error", value = "Произошла ошибка. Пользователь не найден." });
            }

            return Json(new { result = "error", value = "Email или пароль введены некорректно" });
        }

        [Authorize]
        public async Task<IActionResult> Logout()
        {
            await SignInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }

        public IActionResult AccessDenied()
        {
            return View();
        }
    }
}