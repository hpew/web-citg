﻿using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CITGcore.Models;
using Wangkanai.Detection;

namespace CITGcore.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IDevice device;

        public HomeController( IDeviceResolver deviceResolver,ILogger<HomeController> logger)
        {
            _logger = logger;
            device = deviceResolver.Device;
        }

        [HttpGet]
        [Route("/")]
        public IActionResult Index()
        {
            if (device.Type == DeviceType.Desktop)
            {
                return View("Index");
            }
            else
            {
                ViewBag.Layout = 1;
                return View("Index.Mobile");
            }
        }

        [HttpPost]
        [Route("/")]
        public IActionResult Index(int? request)
        {
            if (device.Type == DeviceType.Desktop)
            {
                return PartialView("Index");
            }
            else
            {
                return PartialView("Index.Mobile");
            }
        }

        [HttpGet]
        [Route("Projects")]
        public IActionResult Projects()
        {
            List<Projects> projects = new List<Projects>();
            PartViewIndex part = new PartViewIndex(null);
            ViewBag.part = part;

            if (device.Type == DeviceType.Desktop)
            {
                return View("Projects", projects);
            }
            else
            {
                ViewBag.Layout = 1;
                return View("Projects.Mobile", projects);
            }
        }

        [HttpPost]
        [Route("Projects")]
        public IActionResult Projects(int? request)
        {
            List<Projects> projects = new List<Projects>();
            PartViewIndex part = new PartViewIndex(request);
            ViewBag.part = part;
            
            if (device.Type == DeviceType.Desktop)
            {
                return PartialView("Projects", projects);
            }
            else
            {
                return PartialView("Projects.Mobile", projects);
            }
        }

        [HttpGet]
        [Route("About")]
        public IActionResult About()
        {
            List<Users> users = Instances.Instance.db.GetCollectionUsers();

            Users temp = new Users();

            for (int i = 0; i < users.Count; i++)
            {
                for (int j = 0; j < users.Count; j++)
                {
                    if (users[i].Name.Contains("Поляниченко"))
                    {
                        temp = users[i];
                        users[i] = users[users.Count - 1];
                        users[users.Count - 1] = temp;
                    }

                    else if (users[i].Name.Contains("Жданов"))
                    {
                        temp = users[i];
                        users[i] = users[users.Count - 2];
                        users[users.Count - 2] = temp;
                    }

                    else if (users[i].Name.Contains("Карташов"))
                    {
                        temp = users[i];
                        users[i] = users[0];
                        users[0] = temp;
                    }
                }
            }

            if (device.Type == DeviceType.Desktop)
            {
                return View("About", users);
            }
            else
            {
                ViewBag.Layout = 1;
                return View("About.Mobile", users);
            }  
        }
        
        [HttpPost]
        [Route("About")]
        public IActionResult About(bool partial)
        {
            List<Users> users = Instances.Instance.db.GetCollectionUsers();

            Users temp = new Users();

            for (int i = 0; i < users.Count; i++)
            {
                for (int j = 0; j < users.Count; j++)
                {
                    if (users[i].Name.Contains("Поляниченко"))
                    {
                        temp = users[i];
                        users[i] = users[users.Count - 1];
                        users[users.Count - 1] = temp;
                    }

                    else if (users[i].Name.Contains("Жданов"))
                    {
                        temp = users[i];
                        users[i] = users[users.Count - 2];
                        users[users.Count - 2] = temp;
                    }

                    else if (users[i].Name.Contains("Карташов"))
                    {
                        temp = users[i];
                        users[i] = users[0];
                        users[0] = temp;
                    }
                }
            }

            if (device.Type == DeviceType.Desktop)
            {
                return PartialView("About", users);
            }
            else
            {
                return PartialView("About.Mobile", users);
            }  
        }

        [HttpGet]
        [Route("Contacts")]
        public IActionResult Contacts()
        {
            List<Users> users = Instances.Instance.db.GetCollectionUsersByPhone();
            if (device.Type == DeviceType.Desktop)
            {
                return View("Contacts", users);
            }
            else
            {
                ViewBag.Layout = 1;
                return View("Contacts.Mobile");
            }
        }

        [HttpPost]
        [Route("Contacts")]
        public IActionResult Contacts(bool partial)
        {
            List<Users> users = Instances.Instance.db.GetCollectionUsersByPhone();
            if (device.Type == DeviceType.Desktop)
            {
                return PartialView("Contacts", users);
            }
            else
            {
                return PartialView("Contacts.Mobile");
            }
        }

        [HttpGet]
        [Route("Blog")]
        public IActionResult Blog()
        {
            if (device.Type == DeviceType.Desktop)
            {
                return View("Blog");
            }
            else
            {
                ViewBag.Layout = 1;
                return View("Blog.Mobile");
            }
        }

        [HttpPost]
        [Route("Blog")]
        public IActionResult Blog(bool partial)
        {
            if (device.Type == DeviceType.Desktop)
            {
                return PartialView("Blog");
            }
            else
            {
                return PartialView("Blog.Mobile");
            }
        }

        [Route("Test")]
        public IActionResult Test()
        {
            return View("Test");
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
