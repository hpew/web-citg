﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using CITGcore.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;

namespace AspNetCoreFileUploading
{
    public class UploaderController : Controller
    {
        private IHostingEnvironment hostingEnvironment;

        public UploaderController(IHostingEnvironment hostingEnvironment)
        {
            this.hostingEnvironment = hostingEnvironment;
        }

        [HttpPost]
        [Authorize]
        [Route("AddImage")]
        public async Task<IActionResult> Index(string email, IFormFile image)
        {
            if (email == "")
            {
                email = HttpContext.User.Identity.Name;
            }

            if (image == null || email == null)
            {
                return Json(new { result = "error", value = "Произошла ошибка. Перезагрузите страницу" });
            }
            else
            {
                Users user = Instances.Instance.db.GetUserByEmail(email);
                if (user != null)
                {
                    if (image.ContentType == "image/jpeg" || image.ContentType == "image/png")
                    {
                        string filename = Guid.NewGuid().ToString() + "." + image.ContentType.Replace("image/", "");
                        string path = hostingEnvironment.WebRootPath + @"/UploadImage/" + filename;

                        using var fileStream = new FileStream(path, FileMode.Create);
                        await image.CopyToAsync(fileStream);

                        user.LinkToImg = @"/UploadImage/" + filename;
                        Instances.Instance.db.UpdateUser(user);

                        return Json(new { result = "success", value = "Вы успешно загрузили изображение" });
                    }
                    return Json(new { result = "error", value = "Не корректный формат изображения" });
                }

            }

            return Json(new { result = "error", value = "Не удалось загрузить изображение. Перезагрузите страницу." });
        }

        private string CreateDirectory(string path)
        {
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            return path;
        }
    }
}